{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat alle type-declaraties, instanties en type-gerelateerde functies, zodat we deze makkelijk in meerdere modules kunnen importeren.
-}

{-# LANGUAGE TypeApplications #-}

module Types ( Beats, Hz, Samples, Seconds, Semitones, Track, Ringtone
             , Tone(..), Octave(..), Duration(..), Note(..)
             , Sound, floatSound, intSound, (<+>), asFloatSound, asIntSound, getAsFloats, getAsInts
             , Instrument, instrument, Modifier, modifier, modifyInstrument, arrange
             ) where

import Data.Int (Int32)

type Pulse = [Float]
type Seconds = Float
type Samples = Float
type Hz = Float
type Semitones = Float
type Beats = Float
type Ringtone = String

data Tone = C | CSharp | D | DSharp | E | F | FSharp | G | GSharp | A | ASharp | B deriving (Enum, Eq, Show)
data Octave = Zero | One | Two | Three | Four | Five | Six | Seven | Eight deriving (Enum, Eq, Show)
data Duration = Full | Half | Quarter | Eighth | Sixteenth | Thirtysecond | Dotted Duration deriving (Eq, Show)
data Note = Pause Duration | Note Tone Octave Duration deriving Show

data Sound = IntFrames [Int32] | FloatFrames [Float]
  deriving (Eq, Show)

floatSound :: [Float] -> Sound
floatSound = FloatFrames

intSound :: [Int32] -> Sound
intSound = IntFrames

-- | Semigroup voor het combineren van verschillende sound types. Comineren is twee fragmenten achter elkaar plaatsen.
instance Semigroup Sound where
  (IntFrames x) <> (IntFrames y) = IntFrames (x ++ y)
  (FloatFrames x) <> (FloatFrames y) = FloatFrames (x ++ y)
  (IntFrames _) <> (FloatFrames _) = IntFrames []
  (FloatFrames _) <> (IntFrames _) = IntFrames []

instance Monoid Sound where
  mempty  = IntFrames []

-- | De `<+>` operator combineert twee floatframes en speelt ze daarmee tegelijk af.
(<+>) :: Sound -> Sound -> Sound
(FloatFrames x) <+> (FloatFrames y) = FloatFrames (zipWith (+) x y)
x <+> y = asFloatSound x <+> asFloatSound y

asFloatSound :: Sound -> Sound
asFloatSound (IntFrames fs) = floatSound $ map ( (/ fromIntegral (div (maxBound @Int32 ) 2 )) . fromIntegral ) fs
asFloatSound fframe = fframe

-- | De functie `asIntSound` rond alle getallen af met een map en converteert daarmee een floatFrames naar intFrames.
asIntSound :: Sound -> Sound
asIntSound (FloatFrames fs) = intSound $ map round fs
asIntSound iframe = iframe

getAsFloats :: Sound -> [Float]
getAsFloats sound = case asFloatSound sound of
  (FloatFrames ffs) -> ffs
  _ -> error "asFloatSound did not return FloatFrames"

getAsInts :: Sound -> [Int32]
getAsInts sound = case asIntSound sound of
  (IntFrames ifs) -> ifs
  _ -> error "asIntSound did not return IntFrames"

type Track = (Instrument, [Note])

newtype Instrument = Instrument (Hz -> Seconds -> Pulse)

instrument :: (Hz -> Seconds -> Pulse) -> Instrument
instrument = Instrument

newtype Modifier = Modifier (Pulse -> Pulse)

modifier :: (Pulse -> Pulse) -> Modifier
modifier = Modifier

instance Semigroup Modifier where
  (Modifier m1) <> (Modifier m2) = Modifier $ m1 . m2

-- | De functie `modifiyInstrument` verandert een instrument met een modifier naar een nieuw instrument.
modifyInstrument :: Instrument -> Modifier -> Instrument
modifyInstrument (Instrument i) (Modifier m) = Instrument (\hz duration -> m (i hz duration))

-- | De functie `arrange` past een instrument toe op de frequentie en de lengte.
arrange :: Instrument -> Hz -> Seconds -> Sound
arrange (Instrument i) hz duration = FloatFrames (i hz duration)