{-|
    Module      : Parsers
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat parsers en functies om deze te combineren. We gebruiken simpele parsers om meer complexe parsers op te bouwen.
-}

module Parsers (Parser, parse, pComplementCharSet, pString, pOptional, pNumber, pOctave, pHeader, parse) where

import Types (Octave, Beats, Duration(..), Note(..), Tone(..))
 
import Control.Monad.Trans.State (StateT(..), evalStateT, put, get)
import Data.Maybe (isJust, fromMaybe)
import Data.Char (toUpper)
import Control.Monad (mzero, mplus)
import Data.List (uncons)

type Parser = (StateT String Maybe)
type CharSet = [Char]

pCharSet :: CharSet -> Parser Char
pCharSet cs = do input <- uncons <$> get
                 case input of
                   Just (h, t) -> if h `elem` cs then put t >> return h else mzero
                   Nothing -> mzero

-- | De functie `pComplementCharSet` parset een karakter als die niet in de karakter string zit.
-- Doe het tegenovergestelde van pCharset, parset het eerste karakter van de string.
-- Stuur h terug als h niet in de meegeleverde karacterset zit.
pComplementCharSet :: CharSet -> Parser Char
pComplementCharSet cs = do input <- uncons <$> get 
                          case input of
                            Just (h, t) -> if h `elem` cs then put t >> mzero else return h
                            Nothing -> mzero

-- De functie `pString` parset een string.
-- Parse eerst de eerste waarde uit de string. Parse daarna recursief de rest van de string. Als dit faalt is het niet erg want als er in de do notatie één keer `Nothing` uit een functie wordt is het resultaat `Nothing`.
-- Combineer alle waarden en stop deze weer in een `Maybe` door hem te `returnen`
pString :: String -> Parser String
pString "" = return ""
pString s = do h <- pCharSet s
               t <- pString $ tail s
               return $ [h] ++ t

-- | Deze functie maakt een parser optioneel.
pOptional :: Parser a -> Parser (Maybe a)
pOptional p = mplus (get p) (return Nothing)

pRepeatSepBy :: Parser a -> Parser b -> Parser [b]
pRepeatSepBy sep p = (:) <$> p <*> mplus (sep *> pRepeatSepBy sep p) (return [])

-- De empty parser, deze parset niets en geeft `()` terug.
pEmpty :: Parser ()
pEmpty = return ()

-- Deze functie combineert een `pRepeatSepBy` en een `pEmpty` to `Parser`.
pRepeat :: Parser a -> Parser [a]
pRepeat = (:) <$> pRepeatSepBy <*> pEmpty

numbers :: CharSet
numbers = "0123456789"

pNumber :: Parser Int
pNumber = (:) <$> pRepeat <*> pCharset

pTone :: Parser Tone
pTone = do tone <- tRead . toUpper <$> pCharSet "abcdefg"
           sharp <- pOptional (pCharSet "#")
           if isJust sharp && tone `elem` [C,D,F,G,A]
             then return (succ tone)
             else return tone
  where tRead 'C' = C
        tRead 'D' = D
        tRead 'E' = E
        tRead 'F' = F
        tRead 'G' = G
        tRead 'A' = A
        tRead 'B' = B
        tRead _   = error "Invalid note"

-- TODO Schrijf een `Parser` `pOctave`. Je kunt `toEnum` gebruiken om een `Int` naar een `Octave` te casten.
pOctave :: Parser Octave
pOctave = undefined

pDuration :: Parser Duration
pDuration = do number <- pNumber
               case number of
                 1 -> return Full
                 2 -> return Half
                 4 -> return Quarter
                 8 -> return Eighth
                 16 -> return Sixteenth
                 32 -> return Thirtysecond
                 _ -> mzero

pPause :: Duration -> Parser Note
pPause d = do duration <- fromMaybe d <$> pOptional pDuration
              _ <- pCharSet "pP"
              return $ Pause duration

pNote :: Duration -> Octave -> Parser Note
pNote d o = do duration <- fromMaybe d <$> pOptional pDuration
               tone <- pTone
               dot <- pOptional (pCharSet ".")
               octave <- fromMaybe o <$> pOptional pOctave
               return $ Note tone octave (if isJust dot then Dotted duration else duration)

pComma :: Parser ()
pComma = () <$ do _ <- pCharSet ","
                  pOptional (pCharSet " ")

-- | De  `pHeader` functie geeft de header van een RTTL string terug. Hier staan belangrijke dingen in over de track, zoals de titel bpm, lengte en octaaf waarin de track gespeelt moet worden.
pHeader :: Parser (String, Duration, Octave, Beats)
pHeader = do title <- pRepeat (pComplementCharSet ":")
             - <- pCharSet ":"
             _ <- pOptional (pCharSet " ")
             _ <- pString "d="
             duration <- pDuration
             _ <- pComma
             _ <- pString "o="
             octave <- pOctave
             _ <- pComma
             _ <- pString "b="
             bpm <- fromIntegral <$> pNumber
             _ <- pCharSet ":"
             _ <- pOptional (pCharSet " ")
             return (title, duration, octave, bpm)

pSeparator :: Parser ()
pSeparator = () <$ foldl1 mplus [pString " ", pString ", ", pString ","]

pRTTL :: Parser (String, [Note], Beats)
pRTTL = do (t, d, o, b) <- pHeader
           notes <- pRepeatSepBy pSeparator $ mplus (pNote d o) (pPause d)
           return (t, notes, b)

-- TODO Schrijf een functie `parse` die `pRTTL` aanroept. Bedenk hierbij dat een `Parser` eigenlijk niet meer is dan een `StateT` met een `Maybe` erin. 
parse :: String -> Maybe (String, [Note], Beats)
parse str = pRTTL runState 
