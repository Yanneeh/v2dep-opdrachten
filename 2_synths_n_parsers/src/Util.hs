{-|
    Module      : Types
    Description : Derde checkpoint voor V2DeP: audio-synthesis en ringtone-parsing
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we audio genereren op basis van een abstracte representatie van noten.
    Daarnaast gaan we tekst in het RTTL formaat parsen tot deze abstracte representatie.

    Deze module bevat enkele algemene polymorfe functies.
-}

module Util (zipWithL, zipWithR, comb, fst3, uncurry3) where

import Control.Applicative (liftA2)

-- | Version of ZipWith guaranteed to produce a list of the same length as the second input.
zipWithR :: (a -> b -> b) -> [a] -> [b] -> [b]
zipWithR _ _      []     = []
zipWithR _ []     bs     = bs
zipWithR f (a:as) (b:bs) = f a b : zipWithR f as bs

--- | Een zipWith functie die als input een functie en twee lijsten heeft. Deze functie geeft een lijst terug die dezelfde lengte heeft als de eerste input lijst.
zipWithL :: (a -> b -> a) -> [a] -> [b] -> [a]
zipWithL _ _      []      = []
zipWithL _ as     []      = as -- | Als de tweede input list leeg is geef dan een lege lijst terug. Hierdoor wordt wat over was van de eerste lijst aan het resultaat van de zipWith functie toegevoegd.
zipWithL f (a:as) (b:bs) = f a b : zipWithL f as bs -- | Pas de functie op a en b toe en roep de zipWithL functie recursief aan met de lijsten as en bs.

-- | Use a given binary operator to combine the results of applying two separate functions to the same value. Alias of liftA2 specialised for the function applicative.
comb :: (b -> b -> b) -> (a -> b) -> (a -> b) -> a -> b
comb = liftA2
 
-- | Een functie die de eerste waarde uit een tuple van lengte 3 haalt.
fst3 :: (a, b, c) -> a
fst3 (x, _, _) = x -- | Gebruik pattern matching om de tuple op te splitsen. Alleen het eerste element x van de tuple is belangrijk. Dit element wordt teruggegeven.

-- | Een functie die een functie met drie argumenten omzet naar een functie met een tuple van lengte 3 als argument heeft.
uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
uncurry3 f (x, y, z) = f x y z
