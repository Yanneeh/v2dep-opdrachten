module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | De 'ex1' functie berekent de som van een lijst getallen.
-- Elk getal in de lijst wordt recursief bij elkaar opgeteld.
-- De functie verwacht een lijst met type `Int` en heeft een returnwaarde van type `Int`
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs) = x + ex1 xs

-- | De 'ex2' maakt elk getal in een lijst 1 hoger.
-- Elk getal in de lijst wordt met 1 verhoogt en recursief in een nieuwe lijst gestopt.
-- De functie verwacht een lijst met type `Int` en heeft een returnwaarde van een lijst van type `Int`
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1 : ex2 xs

-- | De 'ex3' functie vereenvoudigt alle elementen van de input lijst met -1.
-- Elk getal in de lijst wordt vermenigvuldigt met -1 en daarna recursief in een nieuwe lijst gestopt.
-- De functie verwacht een lijst met type `Int` en heeft een returnwaarde van een lijst van type `Int`
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 xs

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.

-- | De 'ex4' functie plakt twee lijsten aan elkaar.
-- Er worden hier geen hogere orde functies gebruikt. De elementen van de lijst x worden met recursie vooraan alle waarden van y geplaatst. Dit zorgt ervoor dat de elementen van 
-- De functie verwacht twee lijsten met type `Int` en heeft een returnwaarde van een lijst van type `Int`
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] [] = []
ex4 [] y = y
ex4 (x:xs) y = x : ex4 xs y

-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.

-- | De 'ex5' functie tellen twee lijsten paarsgewijs bij elkaar op.
-- Tel `x` en `y ` bij elkaar op en gebruik recursie om de rest van de lijsten bij elkaar op te tellen.
-- De funtie verwacht twee lijsten van type `Int` en heeft een returnwaarde van lijst type `Int`.

ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (x:xs) (y:ys) = x + y : ex5 xs ys

-- | De 'ex6' functie vermenigvuldigd twee lijsten paarsgewijs met elkaar.
-- Vermenigvuldig `x` en `y` met elkaar en gebruik recursie om de rest van de lijsten met elkaar te vermenigvuldigen.
-- De funtie verwacht twee lijsten van type `Int` en heeft een returnwaarde van lijst type `Int`.
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (x:xs) (y:ys) = x * y : ex6 xs ys

-- | De 'ex7' functie rekent het inwendig product uit.
-- Vermenigvuldig `x` en `y` met elkaar en tel dit op bij het resultaat van het resultaat van dezelfde functie die recursief wordt aangeroepen met de resterende lijsten.
-- De funtie verwacht twee lijsten van type `Int` en heeft een returnwaarde van lijst type `Int`.
ex7 :: [Int] -> [Int] -> Int
ex7 [] [] = 0
ex7 (x:xs) (y:ys) = x * y + ex7 xs ys
