module Examples ( 
    map, zipWith
) where

import Prelude hiding (map, foldr, foldr1, foldl, foldl1, zipWith)

map :: (a -> b) -> [a] -> [b]
map f [] = []
map f (x:xs) = (f x) : map f xs

zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith f [] _ = []
zipWith f _ [] = []
zipwith f (x:xs) (y:ys) = (f x y) : (zipwith f xs ys)

fold :: (a -> a -> a) -> [a] -> a
fold _ [x] = x
fold f (x:xs) = f x (fold f xs)